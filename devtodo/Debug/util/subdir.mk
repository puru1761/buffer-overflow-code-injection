################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../util/CommandArgs.cc \
../util/Lexer.cc \
../util/Regex.cc \
../util/Strings.cc \
../util/Terminal.cc \
../util/XML.cc 

CC_DEPS += \
./util/CommandArgs.d \
./util/Lexer.d \
./util/Regex.d \
./util/Strings.d \
./util/Terminal.d \
./util/XML.d 

OBJS += \
./util/CommandArgs.o \
./util/Lexer.o \
./util/Regex.o \
./util/Strings.o \
./util/Terminal.o \
./util/XML.o 


# Each subdirectory must supply rules for building sources it contributes
util/%.o: ../util/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -m32 -Wno-format-security -fno-stack-protector -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


